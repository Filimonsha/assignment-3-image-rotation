#ifndef ASSIGNMENT_3_IMAGE_ROTATION_STATUS_CODES_H
#define ASSIGNMENT_3_IMAGE_ROTATION_STATUS_CODES_H

enum open_status {
    OPEN_OK = 0,
    OPEN_FAILED_NO_SUCH_FILE
};


enum close_status {
    CLOSE_OK = 0,
    CLOSE_FAILED
};


#endif
