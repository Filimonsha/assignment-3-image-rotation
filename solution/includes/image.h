#ifndef ASSIGNMENT_3_IMAGE_ROTATION_IMAGE_H
#define ASSIGNMENT_3_IMAGE_ROTATION_IMAGE_H
#include <stddef.h>
#include <stdint.h>

struct pixel {
    uint8_t b,r,g;
};

struct image {
    size_t width;
    size_t height;
    struct pixel* data;
};

#endif //ASSIGNMENT_3_IMAGE_ROTATION_IMAGE_H
