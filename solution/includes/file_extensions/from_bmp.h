#ifndef ASSIGNMENT_3_IMAGE_ROTATION_FROM_BMP_H
#define ASSIGNMENT_3_IMAGE_ROTATION_FROM_BMP_H

#include "../image.h"
#include "../image_manager.h"
#include "../status_codes/status_codes_image_basic.h"
#include "bmp.h"

#include <stdio.h>

enum read_status from_bmp(FILE* file, struct image* image);

#endif //ASSIGNMENT_3_IMAGE_ROTATION_FROM_BMP_H
