#ifndef IMAGE_TRANSFORMER_BMP_HEADER_MANAGER_H
#define IMAGE_TRANSFORMER_BMP_HEADER_MANAGER_H
#include "../image.h"
#include "../image_manager.h"
#include "bmp.h"

struct bmp_header create_bmp_header(const struct image * const image);
#endif //IMAGE_TRANSFORMER_BMP_HEADER_MANAGER_H
