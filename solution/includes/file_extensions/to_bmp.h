#ifndef ASSIGNMENT_3_IMAGE_ROTATION_TO_BMP_H
#define ASSIGNMENT_3_IMAGE_ROTATION_TO_BMP_H


#include "../image.h"
#include "../status_codes/status_codes_image_basic.h"
#include "bmp.h"
#include "bmp_header_manager.h"
#include <stdio.h>

enum write_status to_bmp(FILE* file, const struct image* image);

#endif //ASSIGNMENT_3_IMAGE_ROTATION_TO_BMP_H
