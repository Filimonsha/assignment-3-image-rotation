#ifndef ASSIGNMENT_3_IMAGE_ROTATION_IMAGE_BASIC_ACTIONS_H
#define ASSIGNMENT_3_IMAGE_ROTATION_IMAGE_BASIC_ACTIONS_H

#include "file_basic_actions.h"
#include "image.h"
#include "status_codes/status_codes_image_basic.h"

#include <stdio.h>

enum read_status
read_image_from(enum read_status (function_read_of_file_format)(FILE *file, struct image *const first_image),
                FILE* file, const char *filename, struct image *const first_image);

enum write_status
write_image_to(enum write_status (function_write_of_file_format)(FILE *file, const struct image *const second_image),
               FILE* file, const char *filename, const struct image *const second_image);

#endif //ASSIGNMENT_3_IMAGE_ROTATION_IMAGE_BASIC_ACTIONS_H
