#ifndef ASSIGNMENT_3_IMAGE_ROTATION_IMAGE_MANAGER_H
#define ASSIGNMENT_3_IMAGE_ROTATION_IMAGE_MANAGER_H
#include "image.h"
#include <stdlib.h>

struct image init_image(size_t width, size_t height);

void delete_image(struct image* const image);

uint8_t count_padding(size_t width);

struct pixel get_pixel(const struct image * const image, size_t row, size_t column);

void set_pixel(struct image * const image, size_t row, size_t column, struct pixel pixel);

#endif //ASSIGNMENT_3_IMAGE_ROTATION_IMAGE_MANAGER_H
