#ifndef ASSIGNMENT_3_IMAGE_ROTATION_IMAGE_ADVANCED_ACTIONS_H
#define ASSIGNMENT_3_IMAGE_ROTATION_IMAGE_ADVANCED_ACTIONS_H
#include "image.h"
#include "image_manager.h"

struct image rotate(struct image * const image);

#endif //ASSIGNMENT_3_IMAGE_ROTATION_IMAGE_ADVANCED_ACTIONS_H
