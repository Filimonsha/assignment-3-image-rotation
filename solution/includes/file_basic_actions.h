#ifndef ASSIGNMENT_3_IMAGE_ROTATION_FILE_BASIC_ACTIONS_H
#define ASSIGNMENT_3_IMAGE_ROTATION_FILE_BASIC_ACTIONS_H

#include "status_codes/status_codes_file_basic.h"
#include <stdio.h>

enum open_status open_file(FILE **file, const char *filename, const char* mode);

enum close_status close_file(FILE **file);

#endif //ASSIGNMENT_3_IMAGE_ROTATION_FILE_BASIC_ACTIONS_H
