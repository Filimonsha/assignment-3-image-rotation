#include "../../../includes/image_basic_actions.h"

enum read_status
read_image_from(enum read_status (function_read_of_file_format)(FILE *file, struct image * const first_image),
                FILE* file, const char *filename, struct image * const first_image) {
    if (first_image == NULL) return READ_FAILED_INVALID_PIXELS;

    enum open_status file_opened;
    enum read_status image_read;
    enum close_status file_closed;

    file_opened = open_file(&file, filename, "rb");
    if (file_opened == OPEN_FAILED_NO_SUCH_FILE) {
        return READ_FAILED_OPEN;
    }
    image_read = function_read_of_file_format(file, first_image);
    if (image_read != READ_OK) {
        return image_read;
    }
    file_closed = close_file(&file);

    return file_closed == CLOSE_FAILED ? READ_FAILED_CLOSE : READ_OK;
}

enum write_status
write_image_to(enum write_status (function_write_of_file_format)(FILE *file, const struct image * const second_image),
               FILE* file, const char *filename, const struct image * const second_image) {
    if (second_image == NULL) return WRITE_FAILED_PIXELS;

    enum open_status file_opened;
    enum write_status image_written;
    enum close_status file_closed;

    file_opened = open_file(&file, filename, "wb");
    if (file_opened == OPEN_FAILED_NO_SUCH_FILE) {
        return WRITE_FAILED_OPEN;
    }
    image_written = function_write_of_file_format(file, second_image);
    if (image_written != WRITE_OK) {
        return image_written;
    }
    file_closed = close_file(&file);

    return file_closed == CLOSE_FAILED ? WRITE_FAILED_CLOSE : WRITE_OK;
}
