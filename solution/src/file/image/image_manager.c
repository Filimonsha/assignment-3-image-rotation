#include "../../../includes/image_manager.h"

struct image init_image(size_t width, size_t height) {
    struct pixel *pixels = malloc(width * height * sizeof(struct pixel));
    return (struct image) {
            .width = width,
            .height = height,
            .data = pixels
    };
}

void delete_image(struct image *const image) {
    if (image != NULL) {
        free(image->data);
    }
}

uint8_t count_padding(size_t width) {
    uint8_t padding_size = width * sizeof(struct pixel) % 4;
    return padding_size != 0 ? 4 - padding_size : padding_size;
}

struct pixel get_pixel(const struct image *const image, size_t row, size_t column) {
    return image->data[image->width * row + column];
}

void set_pixel(struct image * const image, size_t row, size_t column, const struct pixel pixel) {
    image->data[image->width * row + column] = pixel;
}
