#include "../../../includes/image_advanced_actions.h"

struct image rotate(struct image *const image) {
    if (image == NULL) return (struct image) {0};

    size_t width = image->width;
    size_t height = image->height;

    struct image new_image = init_image(height, width);
    if (new_image.data) {
        for (size_t i = 0; i < height; i++) {
            for (size_t j = 0; j < width; j++) {
                set_pixel(&new_image, j,  height - i - 1, get_pixel(image, i, j));
            }
        }
        return new_image;
    }
    else {
        return (struct image) {0};
    }
}
