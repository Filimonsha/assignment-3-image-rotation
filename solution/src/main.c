#include "../includes/file_extensions/from_bmp.h"
#include "../includes/file_extensions/to_bmp.h"
#include "../includes/image_advanced_actions.h"
#include "../includes/image_basic_actions.h"
#include "../includes/utils/io.h"
#include <stdio.h>

int main( int argc, char** argv ) {
    if (argc != 3) {
        return err_exit("Use 3 parameters to run the program!");
    }
    FILE* first_file = NULL;
    FILE* second_file = NULL;
    struct image first_image = {0};
    struct image second_image = {0};
    const char * const first_file_name = argv[1];
    const char * const second_file_name = argv[2];

    enum read_status first_image_read_status = read_image_from(from_bmp, first_file, first_file_name, &first_image);
    if (first_image_read_status != READ_OK) {
        close_file(&first_file);
        return err_exit("Program failed while reading input file.");
    }

    second_image = rotate(&first_image);
    delete_image(&first_image);

    enum write_status second_image_write_status = write_image_to(to_bmp, second_file, second_file_name, &second_image);
    delete_image(&second_image);
    if (second_image_write_status != WRITE_OK) {
        return err_exit("Program failed while writing output file.");
    }

    return 0;
}



