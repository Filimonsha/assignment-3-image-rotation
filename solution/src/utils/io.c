#include "../../includes/utils/io.h"

int err_exit(const char* const message) {
    fprintf(stderr, "%s", message);
    return 1;
}
